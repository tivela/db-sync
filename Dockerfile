FROM alpine:3.7

# Install MySQL client
RUN set -xe \
    && apk add --no-cache \
        curl \
        mysql-client

# Add scripts
COPY ./sync /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/sync"]