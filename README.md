# db-sync

Copy source db to destination db with user data tables excluded.

## Deployment

```
# deploy configset
kubectl apply -f ./tv2/api-db-sync.properties
# deploy cronjob on prod environment
kubectl apply -f deployment.yml
```
